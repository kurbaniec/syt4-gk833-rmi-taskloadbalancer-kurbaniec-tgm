package client;


import ObserverPattern.Observer;
import compute.Compute;
import compute.Task;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.Socket;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Starts the client program, in which the client request an stub to execute
 * code remotely.
 * <br>
 * The program is command line based, three parameters are required:
 * <br>
 *     1. Server IP-Address <br>
 *     2. Task name or number <br>
 *     3. Parameter of the task <br>
 * <br>
 * @author Kacper Urbaniec
 * @version 16.03.2019
 */
public class ComputeClient  {

    public static void main(String[] args) {
        // Set policy file
        String policyPath = System.getProperty("user.dir");
        policyPath = policyPath.replace("\\", "/");
        policyPath += "/src/main/java/client/client.policy";
        System.setProperty("java.security.policy", policyPath);

        // Check security
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        // Send Compute request to server to get a stub
        try {
            Socket socket = new Socket(args[0], 5555);
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            Object o = in.readObject();
            Compute comp = (Compute) o;

            Task task = new Pi(4);
            try {
                int digits = Integer.parseInt(args[2]);
                if (args[1].matches("-?\\d+(\\.\\d+)?")) {
                    int taskNr = Integer.parseInt(args[1]);
                    if (taskNr == 0) {
                        task = new Pi(digits);
                    } else if (taskNr == 1) {
                        task = new Fibonacci(digits);
                    }
                }
                else {
                    if (args[1].equals("Pi")) {
                        task = new Pi(digits);
                    } else if (args[1].equals("Fibonacci")) {
                        task = new Fibonacci(digits);
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("Something went wrong\n" + ex + "\nUsing Task Pi(4)");
            }
            Object computedOutput = comp.executeTask(task);
            System.out.println(computedOutput);
        } catch (Exception e) {
            System.err.println("ComputePi exception:");
            e.printStackTrace();
        }
    }

}
