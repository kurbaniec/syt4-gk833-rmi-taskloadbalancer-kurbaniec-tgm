package engine;

import ObserverPattern.Subject;
import ObserverPattern.Observer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * This class represents a Loadbalancer-Server for Computing.
 * <br>
 * Clients request {@link ComputeEngine}-stubs for remote code
 * executing. The server redirects the request to a viable {@link ComputeServerWorker},
 * which communicate with a {@link ComputeObserver}. Every {@link ComputeObserver}
 * contains a {@link ComputeEngine} and is responsible for exporting the engine into the
 * RMI-registry.
 * <br>
 * @author Kacper Urbaniec
 * @version 16.03.2019
 */
public class ComputeServer implements Subject {
    private ArrayList<Observer> engines;
    private ArrayList<ComputeServerWorker> workers;
    private ExecutorService executor;
    private String iAddress;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private Thread handler;
    private boolean listening = true;
    private int index = 0;

    public ComputeServer(String iAddress) throws IOException {
        this.executor = Executors.newCachedThreadPool();
        this.iAddress = iAddress;
        this.engines = new ArrayList<>();
        this.workers = new ArrayList<>();
        this.serverSocket = new ServerSocket(5555);
    }

    /**
     * ComputeServer uses notifyObservers for Load-Balancing via
     * Round Robin.
     */
    @Override
    public synchronized void notifyObservers() {
        if (index >= engines.size()) {
            index = 0;
        }
        if (engines.size() != 0) {
            try {
                ComputeServerWorker worker =
                        new ComputeServerWorker(engines.get(index), index, clientSocket, iAddress);
                workers.add(worker);
                executor.submit(worker);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            index++;
        }
        else System.err.println("[Server]: No active Compute Engines");
    }

    /**
     * Starts the whole Server and creates a request Handler for Server-Admin-Input
     * and a loop, that waits for client requests.
     * @param engineCount Specifies how many ComputeObservers with Engines
     *                   should be available
     */
    public void work(int engineCount) {
        // Create engines to Compute
        for(int i = 0; i < engineCount; i++) {
            new ComputeObserver(this);
        }
        System.out.println("[Server]: Started " + engineCount + " ComputeEngines");
        // Creates handler for user input
        this.createHandler();
        this.handler.start();
        // Wait for clients
        // By connection entry notifyObservers
        try {
            while (listening) {
                this.clientSocket = serverSocket.accept();
                this.notifyObservers();
            }
        }
        catch (Exception ex) {
            if(ex instanceof java.net.SocketException) {
                System.out.println("[Server]: Closing Server...");
            }
            else ex.printStackTrace();
        }
    }

    /**
     * Handler that processes user-input on the server.
     * <br>
     * With "exit" the user can close the server. <br>
     * With "add" the user can add a new ComputeObserver <br>
     * With "rm [index]" the user can delete a ComputeObserver at a specified index
     */
    private void createHandler() {
        handler = new Thread(() -> {
            try {
                BufferedReader sysIn = new BufferedReader(new InputStreamReader(System.in));
                String input;
                while (!(input = sysIn.readLine()).equals("exit")) {
                    if(input.equals("add")) {
                        System.out.println("[Server]: Adding new ComputeEngine");
                        new ComputeObserver(this);
                    }
                    else if(input.contains("rm")) {
                        try {
                            String[] rm = input.split(" ");
                            int index = Integer.parseInt(rm[1]);
                            System.out.println("[Server]: Removing ComputeEngine at index " + index);
                            this.removeObserver(engines.get(index));
                        }
                        catch (Exception ex) {
                            System.err.println("Error while removing ComputeEngine");
                            if (engines.size() == 0) {
                                System.err.println("No ComputeEngines to remove!");
                            }
                            else if (ex instanceof IndexOutOfBoundsException) {
                                System.err.println("Index out of bounds!");
                            }
                            else ex.printStackTrace();
                        }
                    }
                    else if(input.equals("ls")) {
                        System.out.println("[Server]: ComputeEngine count - [" + engines.size()+"]");
                    }
                    Thread.sleep(100);
                }
                listening = false;
                executor.shutdown();
                serverSocket.close();
                for(ComputeServerWorker worker: workers) {
                    worker.join(10000);
                    worker.interrupt();
                }
                for(Observer engine: engines) {
                    ((ComputeObserver)engine).removeStub();
                }
                sysIn.close();
            } catch (Exception ex) {ex.printStackTrace();}
        });
    }

    /**
     * Registers a new {@link Observer}.
     * @param o Observer that wants to be registered.
     */
    @Override
    public synchronized void registerObserver(ObserverPattern.Observer o) {
        engines.add(o);
    }

    /**
     * Removes a {@link Observer}.
     * @param o Observer that wants to be removed.
     */
    @Override
    public synchronized void removeObserver(ObserverPattern.Observer o) {
        engines.remove(o);
    }

    /**
     * Starts the Server via command line.
     * <br>
     * At the beginning a RMI-Registry is created. For correct executing the server
     * IP-address is needed.
     * @param args Server IP-address
     */
    public static void main(String[] args) {
        try {
            System.out.println("[Server]: IP-Adress - " + args[0]);
            System.setProperty("java.rmi.server.hostname",args[0]);
            Registry registry = LocateRegistry.createRegistry(1099);
            ComputeServer server = new ComputeServer(args[0]);
            System.out.println("[Server]: ComputeServer bound");
            server.work(Integer.parseInt(args[1]));
        } catch (Exception e) {
            System.err.println("ComputeServer exception:");
            e.printStackTrace();
        }
    }

}
