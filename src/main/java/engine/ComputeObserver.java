package engine;

import ObserverPattern.Observer;
import compute.Compute;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * This class is responsible to create {@link ComputeEngine} stubs,
 * that will be send to clients, so they can execute tasks remotely on them.
 * <br>
 * @author Kacper Urbaniec
 * @version 16.03.2019
 */
public class ComputeObserver implements Observer {
    private ComputeServer server;
    private ComputeServerWorker worker;
    private Compute engine;
    private Compute stub;

    public ComputeObserver(ComputeServer server) {
        this.server = server;
        this.server.registerObserver(this);
    }

    /**
     * This method is called, when the server, more specifically
     * the {@link ComputeServerWorker}-thread demands a
     * {@link ComputeEngine}-stub for a client.
     */
    @Override
    public void update(ComputeServerWorker worker) {
        this.worker = worker;
        if (stub == null) {
            createStub(this.worker.getiAddress());
        }
        this.worker.setOutput(stub);
        this.worker.setProcessing(false);
    }

    private void createStub(String iAddress) {
        String policyPath = System.getProperty("user.dir");
        policyPath = policyPath.replace("\\", "/");
        policyPath += "/src/main/java/engine/server.policy";
        System.setProperty("java.security.policy", policyPath);

        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        System.setProperty("java.rmi.server.hostname", iAddress);
        String name = "Compute";
        engine = new ComputeEngine();
        try {
            System.out.println(worker.getEngineID() + "Creating stub");
            stub = (Compute) UnicastRemoteObject.exportObject(engine, 0);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);
        }
        catch (Exception ex) {
            System.out.println(worker.getEngineID() + ex);
        }
    }

    public void removeStub() {
        if (stub != null) {
            try {
                UnicastRemoteObject.unexportObject(engine, true);
            }
            catch (Exception ex) {ex.printStackTrace();}
        }
    }

}
