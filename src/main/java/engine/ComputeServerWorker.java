package engine;

import ObserverPattern.Observer;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * This class represents a worker thread, to help the server executing
 * specific tasks.
 * <br>
 * ComputeServerWorkers communicate with {@link ComputeObserver}, they
 * request {@link ComputeEngine} from them, so that the worker-thread
 * can send them back to the client.
 * <br>
 * @author Kacper Urbaniec
 * @version 16.03.2019
 */
public class ComputeServerWorker extends Thread {
    private Socket socket;
    private String iAddress;
    private ObjectOutputStream out;
    private Observer engine;
    private String engineID;
    private Object output;
    private boolean processing = true;

    public ComputeServerWorker(
            Observer engine, int engineNumber, Socket socket, String iAddress
    ) throws IOException {
        this.iAddress = iAddress;
        this.engine = engine;
        this.engineID = "[Engine " + engineNumber + "]: ";
        this.socket = socket;
        this.out = new ObjectOutputStream(socket.getOutputStream());
    }

    /**
     * Request from a {@link ComputeObserver} a {@link ComputeEngine} stub,
     * that will be send to the client.
     */
    @Override
    public void run() {
        System.out.println(engineID + "Setting ComputeEngine up");
        engine.update(this);
        System.out.println(engineID + "Starting Sending Process");
        try {
            while (processing) {
                Thread.sleep(100);
            }
            if (output != null) {
                    System.out.println(engineID + "Sending!");
                    out.writeObject(output);
            }
            else {
                System.out.println(engineID + "Engine output not accessible!");
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public Object getOutput() {
        return output;
    }

    public void setOutput(Object output) {
        this.output = output;
    }

    public void setProcessing(boolean processing) {
        this.processing = processing;
    }

    public String getEngineID() {
        return engineID;
    }

    public String getiAddress() {
        return iAddress;
    }
}
