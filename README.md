# Distributed Computing "*RMI Task Loadbalancer*" 

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

### Code ausführen

Zur Ausführung wird `gradle` verwendet.

Zuerst sollte `ComputeEngine` gestartet werden, die "Ziel-JVM" wo das Remote-Object ausgeführt werden soll:

```bash
gradle engine
```

Dann kann beispielsweise `ComputePi` gestartet werden, um die Pi-Nachkommastellen zu berechnen. Als Parameter  muss die Netz-Adresse der "Ziel-JVM" also sozusagen des Servers und die gewünschten Pi-Nachkommastellen angegeben werden.

```bash
gradle compute --args="localhost 1234"
```

bzw.

```bash
gradle computePi --args="localhost 1234"
```

Seit neuestem kann man auch die Fibonacci-Zahl berechnen. Die Parameter sind ähnlich wie beim Pi, nur diesmal gibt der zweite Parameter das Bindeglied n an, welches berechnet werden soll.

```bash
gradle computeFibonacci --args="localhost 50" 
```

### Loadbalancer ausführen

Loadbalancer-Server starten:

```bash
gradle computeServer --args="[Server IP-Adresse] [Anzahl von Engines]"
```

Client starten:

```bash
gradle computeClient --args="[Server IP-Addresse] [Task-Name oder Nummer] [Task-Parameter]"
// Pi Task Beispiele
gradle computeClient --args="Pi 10" 
gradle computeClient --args="0 10" //Alternative call
// Fibonacci Task Beispiele
gradle computeClient --args="Fibonacci 10" 
gradle computeClient --args="0 10" //Alternative call
```

Für weiter Personalisierung siehe [build.gradle](build.gradle).

### Latex-Protokoll

Overleaf-Version [hier](https://www.overleaf.com/read/gzrmfsrwfsqt) zu finden.

Letzte PDF-Version ist im Repository unter [**4AHIT_SYT_Distributed_Computing.pdf**](4AHIT_SYT_Distributed_Computing.pdf) zu finden.

## Quellen

* siehe [Latex-Protokoll](https://www.overleaf.com/read/gzrmfsrwfsqt)